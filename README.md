# CarFeatures





## Notes


- Existing Data Sets
    - http://mmlab.ie.cuhk.edu.hk/datasets/comp_cars/index.html
    - http://ai.stanford.edu/~jkrause/cars/car_dataset.html
- Easiest tool for converting ML models: RoboFlow
- CVAT for annotations is a pain, resulting models also need conversion to CoreML


- Car Parts Repo: https://github.com/dsmlr/Car-Parts-Segmentation
- Roboflow trained model: https://app.roboflow.com/car-ybu4z/carfeatures2/2
